package flexodbc

import (
	"database/sql"
	"database/sql/driver"
	"encoding/json"
	"strconv"
	"time"

	"github.com/shopspring/decimal"
)

var DefualtPrecision int32 = 4

func NewFlexString(s string) FlexString {
	return FlexString{value: s, Valid: true}
}

type FlexString struct {
	Valid bool
	value string
}

func (n FlexString) String() string {
	return n.value
}

func (n FlexString) MarshalJSON() ([]byte, error) {
	if n.Valid {
		return json.Marshal(n.value)
	}

	return []byte("null"), nil
}

func (f *FlexString) Scan(src interface{}) error {

	if src == nil {
		f.value = "null"
		f.Valid = false

	} else {

		var raw []byte

		raw, f.Valid = src.([]byte)

		if f.Valid {
			f.value = string(raw)
		}
	}

	return nil
}

func (f FlexString) Value() (driver.Value, error) {
	if !f.Valid {
		return nil, nil
	}
	return driver.Value(f.value), nil
}

func (n *FlexString) UnmarshalJSON(data []byte) error {
	if string(data) == "null" {
		n.Valid = false
		n.value = ""
		return nil
	}
	n.Valid = true
	return json.Unmarshal(data, &n.value)
}

func (f *FlexString) Set(v string) {
	f.Valid = true
	f.value = v
}

type FlexFloat64 sql.NullFloat64

func (f *FlexFloat64) Scan(src interface{}) error {
	f.Float64, f.Valid = src.(float64)

	return nil
}

func (n FlexFloat64) String() string {
	if !n.Valid {
		return "null"
	}
	return strconv.FormatFloat(n.Float64, 'G', -1, 64)
}

func (n FlexFloat64) MarshalJSON() ([]byte, error) {
	if n.Valid {
		return json.Marshal(n.Float64)
	}

	return []byte("null"), nil
}

func (n *FlexFloat64) UnmarshalJSON(data []byte) error {
	if string(data) == "null" {
		n.Valid = false
		return nil
	}
	n.Valid = true
	return json.Unmarshal(data, &n.Float64)
}

func (f *FlexFloat64) Set(v float64) {
	f.Valid = true
	f.Float64 = v
}

func NewFlexDate(t time.Time) FlexDate {
	return FlexDate{
		Time:  t,
		Valid: true,
	}
}

type FlexDate struct {
	time.Time
	Valid bool
}

func (f *FlexDate) Set(v time.Time) {
	f.Valid = true
	f.Time = v
}

func (t FlexDate) String() string {
	if !t.Valid {
		return "null"
	}
	return t.Time.String()
}

func (t FlexDate) Value() (driver.Value, error) {
	if !t.Valid {
		return nil, nil
	}
	return driver.Value(t.Format("{d '2006-01-02'}")), nil
}

func (t *FlexDate) Scan(src interface{}) error {

	t.Time, t.Valid = src.(time.Time)

	return nil
}

func (t FlexDate) MarshalJSON() ([]byte, error) {
	if t.Valid {
		return json.Marshal(t.Time.Format("2006-01-02"))
	}

	return []byte("null"), nil
}

func (t *FlexDate) UnmarshalJSON(data []byte) error {
	var err error
	if string(data) == "null" {
		t.Valid = false
		return nil
	}

	var v string

	err = json.Unmarshal(data, &v)

	if err != nil {
		return err
	}

	t.Time, err = time.Parse("2006-01-02", v)

	if err == nil {
		t.Valid = true
	}

	return err
}

func NewFlexNumeric(d decimal.Decimal) FlexNumeric {
	return FlexNumeric{
		d,
		true,
		DefualtPrecision,
	}
}

type FlexNumeric struct {
	decimal.Decimal

	Valid     bool
	Precision int32
}

func (n *FlexNumeric) Scan(value interface{}) error {
	if value == nil {
		n.Decimal, n.Valid = decimal.Zero, false
		return nil
	}

	f, isFloat := value.(float64)

	if isFloat {
		n.Decimal = decimal.NewFromFloat(f)
		n.Valid = true
		n.Precision = n.Decimal.Exponent() * -1
		return nil
	}

	return n.Decimal.Scan(value)
}

func (n FlexNumeric) Value() (driver.Value, error) {
	if !n.Valid {
		return nil, nil
	}
	f, _ := n.Decimal.Round(n.Precision).Float64()

	return driver.Value(f), nil
}

func (n FlexNumeric) MarshalJSON() ([]byte, error) {
	if n.Valid {
		f, _ := n.Decimal.Round(n.Precision).Float64()

		return json.Marshal(f)
	}

	return []byte("null"), nil
}

func (n *FlexNumeric) UnmarshalJSON(data []byte) error {
	if string(data) == "null" {
		n.Valid = false
		return nil
	}

	n.Valid = true

	var f float64

	err := json.Unmarshal(data, &f)

	n.Decimal = decimal.NewFromFloat(f)
	n.Precision = n.Decimal.Exponent() * -1

	return err
}

func (f *FlexNumeric) FromFloat(value float64) *FlexNumeric {
	f.Decimal = decimal.NewFromFloat(value)
	f.Precision = f.Decimal.Exponent() * -1

	return f
}

func (f *FlexNumeric) FromFloatWithExponent(value float64, exp int32) *FlexNumeric {
	f.Decimal = decimal.NewFromFloatWithExponent(value, exp)
	f.Precision = exp * -1

	return f
}
