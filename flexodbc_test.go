package flexodbc

import (
	"database/sql/driver"
	"encoding/json"
	"errors"
	"time"

	. "github.com/onsi/ginkgo"
	"github.com/shopspring/decimal"
)

var _ = Describe("Testing with Ginkgo", func() {

	It("FlexString_JSON_Marshal", func() {
		t := GinkgoT()

		tests := []struct {
			input    FlexString
			expected []byte
			err      error
		}{
			{FlexString{value: "test", Valid: true}, []byte(`"test"`), nil},
			{FlexString{value: "", Valid: false}, []byte(`null`), nil},
		}

		for _, test := range tests {

			actual, err := json.Marshal(test.input)

			if err != test.err {
				t.Errorf("Unexpected error. Expected %s got %s", test.err, err)
			}

			if string(actual) != string(test.expected) {
				t.Errorf("Expected %#v got %#v", test.expected, actual)
			}

		}

	})

	It("FlexString_JSON_Unmarshal", func() {
		t := GinkgoT()

		tests := []struct {
			expected FlexString
			input    []byte
			err      error
		}{
			{FlexString{value: "test", Valid: true}, []byte(`"test"`), nil},
			{FlexString{value: "", Valid: false}, []byte(`null`), nil},
		}

		for _, test := range tests {

			var actual FlexString
			err := json.Unmarshal(test.input, &actual)

			if err != test.err {
				t.Errorf("Unexpected error. Expected %s got %s", test.err, err)
			}

			if actual != test.expected {
				t.Errorf("Expected %#v got %#v", test.expected, actual)
			}

		}

	})

	It("FlexFloat64_JSON_Marshal", func() {
		t := GinkgoT()

		tests := []struct {
			input    FlexFloat64
			expected []byte
			err      error
		}{
			{FlexFloat64{Float64: 0.1, Valid: true}, []byte(`0.1`), nil},
			{FlexFloat64{Float64: 0, Valid: false}, []byte(`null`), nil},
		}

		for _, test := range tests {

			actual, err := json.Marshal(test.input)

			if err != test.err {
				t.Errorf("Unexpected error. Expected %s got %s", test.err, err)
			}

			if string(actual) != string(test.expected) {
				t.Errorf("Expected %#v got %#v", test.expected, actual)
			}
		}

	})

	It("FlexFloat64_JSON_Unmarshal", func() {
		t := GinkgoT()

		tests := []struct {
			expected FlexFloat64
			input    []byte
			err      error
		}{
			{FlexFloat64{Float64: 0.1, Valid: true}, []byte(`0.1`), nil},
			{FlexFloat64{Float64: 0, Valid: false}, []byte(`null`), nil},
		}

		for _, test := range tests {

			var actual FlexFloat64

			err := json.Unmarshal(test.input, &actual)

			if err != test.err {
				t.Errorf("Unexpected error. Expected %s got %s", test.err, err)
			}

			if actual != test.expected {
				t.Errorf("Expected %#v got %#v", test.expected, actual)
			}
		}

	})

	It("FlexDate_JSON_Marshal", func() {
		t := GinkgoT()

		dateString := `"2021-01-01"`
		date, _ := time.Parse(`"2006-01-02"`, dateString)

		tests := []struct {
			input    FlexDate
			expected []byte
			err      error
		}{
			{FlexDate{Time: date, Valid: true}, []byte(dateString), nil},
			{FlexDate{Valid: false}, []byte(`null`), nil},
		}

		for _, test := range tests {

			actual, err := json.Marshal(test.input)

			if err != test.err {
				t.Errorf("Unexpected error. Expected %s got %s", test.err, err)
			}

			if string(actual) != string(test.expected) {
				t.Errorf("Expected %#v got %#v", test.expected, actual)
			}
		}
	})

	It("FlexDate_JSON_Unmarshal", func() {
		t := GinkgoT()

		dateString := `"2021-01-01"`
		date, _ := time.Parse(`"2006-01-02"`, dateString)

		tests := []struct {
			expected FlexDate
			input    []byte
			err      error
		}{
			{FlexDate{Time: date, Valid: true}, []byte(dateString), nil},
			{FlexDate{Valid: false}, []byte(`null`), nil},
			{FlexDate{Valid: false}, []byte(`0.1`), errors.New("json: cannot unmarshal number into Go value of type string")},
			{FlexDate{Valid: false}, []byte(`"invalid-date"`), errors.New(`parsing time "invalid-date" as "2006-01-02": cannot parse "invalid-date" as "2006"`)},
		}

		for _, test := range tests {

			var actual FlexDate
			err := json.Unmarshal(test.input, &actual)

			// Really hacky check of error strings. Not sure how to check JSON error types
			if (test.err == nil && err != nil) || (err != nil && test.err.Error() != err.Error()) {
				t.Errorf("Unexpected error. Expected %s got %s, %T", test.err, err, err)
			}

			if actual != test.expected {
				t.Errorf("Expected %#v got %#v", test.expected, actual)
			}

		}
	})

	It("FlexDate_Valuer", func() {
		t := GinkgoT()

		dateString := `"2021-01-01"`
		date, _ := time.Parse(`"2006-01-02"`, dateString)

		tests := []struct {
			s FlexDate
			e driver.Value
		}{
			{FlexDate{Time: date, Valid: true}, driver.Value("{d '2021-01-01'}")},
			{FlexDate{Valid: false}, nil},
		}

		for _, test := range tests {

			val, err := test.s.Value()

			if err != nil {
				t.Error(err)
			}

			if val != test.e {
				t.Errorf("Expected %v got %v", test.e, val)
			}
		}
	})

	It("FlexDate_Scanner", func() {
		t := GinkgoT()

		dateString := `"2021-01-01"`
		date, _ := time.Parse(`"2006-01-02"`, dateString)

		tests := []struct {
			expected FlexDate
			input    interface{}
		}{
			{FlexDate{Time: date, Valid: true}, date},
			{FlexDate{Valid: false}, nil},
		}

		for _, test := range tests {

			testDate := FlexDate{}

			err := testDate.Scan(test.input)

			if err != nil {
				t.Error(err)
			}

			if testDate != test.expected {
				t.Errorf("Expected %#v got %#v", test.expected, testDate)
			}
		}
	})

	It("FlexNumeric_JSON_Marshal", func() {
		t := GinkgoT()

		tests := []struct {
			input    FlexNumeric
			expected []byte
			err      error
		}{
			{FlexNumeric{decimal.NewFromFloat(0.1), true, 2}, []byte(`0.1`), nil},
			{FlexNumeric{decimal.NewFromFloat(0.1), false, 2}, []byte(`null`), nil},
			{FlexNumeric{decimal.NewFromFloat(0.1234), true, 2}, []byte(`0.12`), nil},
		}

		for _, test := range tests {

			actual, err := json.Marshal(test.input)

			if err != test.err {
				t.Errorf("Unexpected error. Expected %s got %s", test.err, err)
			}

			if string(actual) != string(test.expected) {
				t.Errorf("Expected %s got %s, %#v", test.expected, actual, test)
			}
		}

	})

	It("FlexNumeric_JSON_Unmarshal", func() {
		t := GinkgoT()

		tests := []struct {
			expected FlexNumeric
			input    []byte
			err      error
		}{
			{FlexNumeric{decimal.NewFromFloat(0.1), true, 1}, []byte(`0.1`), nil},
			{FlexNumeric{decimal.NewFromFloat(0), false, 1}, []byte(`null`), nil},
			{FlexNumeric{decimal.NewFromFloat(0.1234), true, 4}, []byte(`0.1234`), nil},
			{FlexNumeric{decimal.NewFromFloat(0.12), true, 2}, []byte(`0.12`), nil},
		}

		for _, test := range tests {

			var actual FlexNumeric

			err := json.Unmarshal(test.input, &actual)

			if err != test.err {
				t.Errorf("Unexpected error. Expected %s got %s", test.err, err)
			}

			if actual.Valid != test.expected.Valid || !actual.Decimal.Equals(test.expected.Decimal) {
				t.Errorf("Expected %s got %s. (%#v != %#v)", test.expected, actual, test.expected, actual)
			}
		}

	})

	It("FlexNumeric_Valuer", func() {
		t := GinkgoT()

		tests := []struct {
			s   FlexNumeric
			e   driver.Value
			err error
		}{
			{FlexNumeric{decimal.NewFromFloat(0.1), true, 1}, driver.Value(0.1), nil},
			{FlexNumeric{decimal.NewFromFloat(0), false, 1}, driver.Value(nil), nil},
			{FlexNumeric{decimal.NewFromFloat(0.1234), true, 4}, driver.Value(0.1234), nil},
			{FlexNumeric{decimal.NewFromFloat(0.12), true, 2}, driver.Value(0.12), nil},
		}

		for _, test := range tests {

			val, err := test.s.Value()

			if err != test.err {
				t.Error(err)
			}

			if val != test.e {
				t.Errorf("Expected %#v got %#v", test.e, val)
			}
		}
	})

	It("FlexNumeric_Scanner", func() {

		tests := []struct {
			expected FlexNumeric
			input    interface{}
			err      error
		}{
			{FlexNumeric{decimal.NewFromFloat(0.1), true, 1}, 0.1, nil},
			{FlexNumeric{decimal.NewFromFloat(0), false, 1}, nil, nil},
			{FlexNumeric{decimal.NewFromFloat(0.1234), true, 4}, 0.1234, nil},
			{FlexNumeric{decimal.NewFromFloat(0.12), true, 2}, 0.12, nil},
		}

		for _, test := range tests {

			actual := FlexNumeric{}

			err := actual.Scan(test.input)

			if err != test.err {
				GinkgoT().Errorf("Unexpected error. Expected %s got %s", test.err, err)
			}

			if actual.Valid != test.expected.Valid || actual.String() != test.expected.String() {
				GinkgoT().Errorf("Expected %s got %s. %#v", test.expected.String(), actual.String(), test)
			}
		}
	})
})
